import { AUser } from "ajato";
import { SchemaDefinition } from "mongoose";

class MUser extends AUser {

    schemaDefinition(){
        const schema:SchemaDefinition = {
            lastname: {
                type: String,
                required: true
            }
        }
        return {...super.schemaDefinition(), ...schema}
    }

}

export = MUser
