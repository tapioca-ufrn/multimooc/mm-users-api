import ajato, { Ajato, AModel, Document } from 'ajato'

interface IPost extends Document {
  title: string
  body: string
}

class PostModel extends AModel {
  name = () => 'Posts'
  schemaDefinition() {
    return {
      title: {
        type: String,
        required: true
      },
      body: {
        type: String,
        required: true
      }
    }
  }
}

export = PostModel






// postSchema.post('save', function(doc:IPost, next) {
//   doc.title = doc.title + " - post esteve"
//   next()
// })

// export default ajato().db().model<IPost>('Posts', postsSchema)
