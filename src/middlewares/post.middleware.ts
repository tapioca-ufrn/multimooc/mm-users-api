import {Router, Response, Request, NextFunction} from 'express'
import {Middleware} from 'ajato'

class PostMiddleware implements Middleware {

  add(router:Router){
    this.addChangeTitle(router)
  }

  private addChangeTitle(router:Router){
    console.log('cadastrando midware')
    router.post('/', function (req:Request, res:Response, next:NextFunction) {
      console.log('Called my middleware')
      req.body.body = req.body.body + ' - O middleware esteve aqui!'
      next()
    })
  }

}

export = PostMiddleware
