import ajato, {Ajato, RouterBuilder, CrudController, AuthjwtController} from 'ajato'
import PostsModel from './models/posts.model'
import MUser from './models/user.model' 
import PostMiddleware from './middlewares/post.middleware'

const app:Ajato = ajato()

app.use('/posts', new RouterBuilder()
    .add(new PostMiddleware())
    .add(new CrudController(new PostsModel()))
    .routes())

app.use('/', new RouterBuilder()
    .add(new AuthjwtController(new MUser()))
    .routes())

app.use('/user', new RouterBuilder()
    .add(new CrudController(new MUser()))
    .routes())

app.start()
















// import * as dotenv from 'dotenv';
// const result = dotenv.config()
// import express, {Application, Request, Response, NextFunction} from 'express'
// // import GenericCrudMiddleware from './arq/generic-crud.middleware'
// // import AuthJWTRouter from './arq/middleware/authjwt.middleware'
//
// const actions = require('mongoose-rest-actions')
// const mquery = require('express-mquery')
// const datasource = require('./datasource/datasource.mongo')
//

// const PORT:number = Number(process.env.PORT)

// //Configurations
// //Models
//
// import userModel from './models/user.model'
//
// //Middlewares
// import PostsMiddleware from './middlewares/posts.middleware'
// import RootController from './middlewares/root.middleware'
//
// // //Adding to Express
// import RouterBuilder from './arq/routerBuilder'
//
// app.use('/', new RouterBuilder(express.Router())
//     .add(new RootController())
//     .add(new AuthJWTRouter())
//     .routes())
//
// app.use('/user', new RouterBuilder(express.Router())
//     .add(new CrudController(userModel))
//     .routes())
//
// app.use('/posts', new RouterBuilder(express.Router())
//     .add(new PostsMiddleware())
//     .add(new CrudController(postsModel))
//     .routes())
//
